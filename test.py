import unittest


class Calculator:
    """Производит различные арифметические действия."""

    def summ(self, *args):
        """Возвращает сумму принятых аргументов."""
        if len(args) < 2:
            return None
        else:
            return sum(i for i in args)


class TestCalc(unittest.TestCase):

    def setUp(self):
        self.calc = Calculator()

    def test_summ(self):

        call = self.calc.summ(5, 4)
        self.assertEqual(call, 9, 'Функция summ работает верно')

    def test_summ_without_arg(self):
        call = self.calc.summ()
        self.assertIsNone(call, 'Неверная работа функции summ без аргументов')

    def test_summ_with_one_arg(self):
        call = self.calc.summ(5)
        self.assertIsNone(call, 'Неверная работа функции summ с одним аргументом ')


if __name__ == '__main__':
    unittest.main()
